import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  childEventText = '';

  eventFromChild(e: string) {
    this.childEventText = `Got event from child: ${e}`;
  }
}
