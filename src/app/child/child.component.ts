import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.scss'],
})
export class ChildComponent {
  @Output() eventFromChild = new EventEmitter<string>();

  onClick(text: string) {
    this.eventFromChild.emit(text);
  }
}
